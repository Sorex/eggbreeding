package com.cyprias.EggBreeding.listeners;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.inventory.ItemStack;

import org.bukkit.plugin.java.JavaPlugin;

import com.cyprias.EggBreeding.ChatUtils;
import com.cyprias.EggBreeding.Logger;
import com.cyprias.EggBreeding.Perm;
import com.cyprias.EggBreeding.Plugin;
import com.cyprias.EggBreeding.configuration.Config;

public class EntityListener  implements Listener {
	static public void unregisterEvents(JavaPlugin instance) {
		CreatureSpawnEvent.getHandlerList().unregister(instance);
	}
	
	
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onCreatureSpawn(CreatureSpawnEvent event) {
		// Don't do anything if another plugin has canceled the event.
		if (event.isCancelled())
			return;

		// Only affect mobs spawned by breeding.
		if (!event.getSpawnReason().equals(SpawnReason.BREEDING))
			return;

		// Check if we should ingore this mob.
		List<String> blacklist = Config.getStringList("mobBlacklist");
		for (String mob : blacklist){
			if (event.getEntityType().toString().equalsIgnoreCase(mob)){
				Logger.debug("Exiting breed due to blacklisted " + mob);
				return;
			}
		}
		
		// Check if we should affect this world.
		Location loc = event.getLocation();
		List<String> worlds = Config.getStringList("worlds");
		if (worlds.size() > 0){
			for (int i=worlds.size()-1;i>=0;i--){
				Logger.debug("Checking " + i + ": " + worlds.get(i));
				
				if (worlds.get(i).equalsIgnoreCase(loc.getWorld().getName())){
					break;
				}else if (i == 0)
					return;
			}
		}
		
		// Flip a coin, see if we should drop an egg or not.
		double rand = Math.random();
		if (rand > Config.getDouble("properties.egg-chance")) {
			Logger.debug("chance = " + (rand*100) + ", exiting...");
			return;
		}
		
		int d = Config.getInt("properties.required-player-distance");
		if (d > 0){
			List<Entity> ents = event.getEntity().getNearbyEntities(d, d, d);
			
			for (int i=0; i < ents.size(); i ++){
				if (ents.get(i).getType().equals(EntityType.PLAYER)){
					Player p = (Player) ents.get(i);
					if (Plugin.hasPermission(p, Perm.DROP_DISTANCE)){
						Logger.debug(p.getName() + " is near enough.");
						break;
					}
				}
				else if (i == (ents.size()-1)){
					Logger.debug("No players near, not dropping an egg.");
					return;
				}
			}
			
		}
		

		d = Config.getInt("properties.announce-drop-radius");
		if (d > 0){
			List<Entity> ents = event.getEntity().getNearbyEntities(d, d, d);
			for (int i=0; i < ents.size(); i ++){
				if (ents.get(i).getType().equals(EntityType.PLAYER)){
					Player p = (Player) ents.get(i);
					ChatUtils.send(p, ChatColor.GRAY+"A " + ChatColor.WHITE + event.getEntityType().toString().toLowerCase() + ChatColor.GRAY+" egg has dropped nearby.");
					
				}
			}
			
		}
		
		Logger.info("Dropping " + event.getEntityType().toString().toLowerCase() + " egg at "+getStringLoc(loc) + ".");
		
		// Our egg to drop.
		ItemStack egg = new ItemStack(383);//MONSTER_EGG

		//Set the egg type to match the mob's type.
		egg.setDurability(event.getEntityType().getTypeId());
		
		// Drop the egg at the breed location.
		loc.getWorld().dropItemNaturally(loc, egg);
		Logger.debug("Dropping egg at breed location.");
		
		//Cancel the spawning of the baby mob.
		event.setCancelled(true);
	}
	
	
	String getStringLoc(Location loc){
		String s = loc.getWorld().getName();
		s += " " + loc.getBlockX();
		s += " " + loc.getBlockY();
		s += " " + loc.getBlockZ();
		return s;
	}
	
	
}
