package com.cyprias.EggBreeding;

import java.util.HashMap;

import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.permissions.PermissionDefault;
import org.bukkit.plugin.PluginManager;

public enum Perm {
	DROP_DISTANCE("EggBreeding.drop-distance", PermissionDefault.TRUE), //Default to everyone having access.
	PARENT_USER("EggBreeding.*", DROP_DISTANCE);
	
	
	private Perm(String perm, PermissionDefault permDefault) {
		this(perm, String.format(DEFAULT_ERROR_MESSAGE, perm), permDefault);
	}

	private Perm(String value, Perm... childrenArray) {
		this(value, String.format(DEFAULT_ERROR_MESSAGE, value), childrenArray);
	}

	private Perm(String perm, String errorMess) {
		this(perm, errorMess, PermissionDefault.OP);
	}

	private Perm(String perm, String errorMess, PermissionDefault permDefault) {
		this.permission = perm;
		this.errorMessage = errorMess;
		this.bukkitPerm = new Permission(permission, permDefault);
	}
	
	private Perm(String value, String errorMess, Perm... childrenArray) {
		this(value, String.format(DEFAULT_ERROR_MESSAGE, value));
		for (Perm child : childrenArray) {
			child.setParent(this);
		}
	}

	public static HashMap<String, PermissionAttachment> permissions = new HashMap<String, PermissionAttachment>();

	private final String permission;
	public static final String DEFAULT_ERROR_MESSAGE = "You do not have access to %s";

	public Perm getParent() {
		return parent;
	}

	private final Permission bukkitPerm;
	private Perm parent;
	private final String errorMessage;

	private void setParent(Perm parentValue) {
		if (this.parent != null)
			return;
		this.parent = parentValue;
	}

	public String getPermission() {
		return permission;
	}

	public void loadPermission(PluginManager pm) {
		pm.addPermission(bukkitPerm);
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void unloadPermission(PluginManager pm) {
		pm.removePermission(bukkitPerm);
	}
}
