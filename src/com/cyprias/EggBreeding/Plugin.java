package com.cyprias.EggBreeding;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcstats.Metrics;

import com.cyprias.EggBreeding.configuration.Config;
import com.cyprias.EggBreeding.configuration.YML;
import com.cyprias.EggBreeding.listeners.EntityListener;


public class Plugin extends JavaPlugin {
	private static Plugin instance = null;

	public void onEnable() {
		instance = this;
		// Check if config.yml exists on disk, copy it over if not. This keeps our comments intact.
		if (!(new File(getDataFolder(), "config.yml").exists())) {
			Logger.info("Copying config.yml to disk.");
			try {
				YML.toFile(getResource("config.yml"), getDataFolder(), "config.yml");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				return;
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		}

		//Check if the config on disk is missing any settings, tell console if so.
		try {
			Config.checkForMissingProperties();
		} catch (IOException e4) {
			e4.printStackTrace();
		} catch (InvalidConfigurationException e4) {
			e4.printStackTrace();
		}
		
		//Register our event listeners. 
		registerListeners(new EntityListener());
		
		// Start plugin metrics, see how popular our plugin is.
		if (Config.getBoolean("properties.use-metrics")){
			try {
				new Metrics(this).start();
			} catch (IOException e) {}
		}
		
		// Check if there's a new version available, notify console if so. 
		if (Config.getBoolean("properties.check-new-version"))
			checkVersion();
		
		Logger.info("enabled.");
	}

	
	public void onDisable() {
		EntityListener.unregisterEvents(instance);
		instance.getServer().getScheduler().cancelTasks(instance);
		instance = null;
		Logger.info("disabled.");
	}

	
	public static void reload() {
		instance.reloadConfig();
	}

	public static final Plugin getInstance() {
		return instance;
	}
	
	Listener[] listenerList;
	private void registerListeners(Listener... listeners) {
		PluginManager manager = getServer().getPluginManager();

		listenerList = listeners;

		for (Listener listener : listeners) {
			manager.registerEvents(listener, this);
		}
	}
	private void checkVersion() {
		// Run our version checker in async thread, so not to lockup if timeout.
		getServer().getScheduler().runTaskAsynchronously(instance, new Runnable() {
			public void run() {
				try {
					VersionChecker version = new VersionChecker("http://dev.bukkit.org/server-mods/eggbreeding/files.rss");
					VersionChecker.versionInfo info = (version.versions.size() > 0) ? version.versions.get(0) : null;
					if (info != null) {
						String curVersion = getDescription().getVersion();
						if (VersionChecker.compareVersions(curVersion, info.getTitle()) < 0) {
							Logger.warning("We're running v" + curVersion + ", v" + info.getTitle() + " is available");
							Logger.warning(info.getLink());
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				} catch (ParserConfigurationException e) {
					e.printStackTrace();
				}

			}
		});
	}
	
	
	static public boolean hasPermission(CommandSender sender, Perm permission) {
		if (sender != null) {
			if (sender instanceof ConsoleCommandSender)
				return true;

			if (sender.hasPermission(permission.getPermission())) {
				return true;
			} else {
				Perm parent = permission.getParent();
				return (parent != null) ? hasPermission(sender, parent) : false;
			}
		}
		return false;
	}
}
